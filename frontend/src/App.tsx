import React from 'react';
import Header from './components/commons/Header';

function App() {
  return (
    <div className="general-container">
      <Header />
      <h1>BASIC PROJECT</h1>
    </div>
  );
}

export default App;
